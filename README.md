# Command line Interface

A simple Command line interface developed using Java.
    
    Commands: 
            1) dir -> Displays the contents of the current folder.
            2) time -> Displays the current date and time on the screen.
            3) copy /+Y <source_file> <destination_file> -> Copies the source file to the destination file.
            4) type *.* -> Displays all text files in the current folder.
            5) sort <file_name> -> Reads 32 binary numbers from the file 'file_name' and displays them sorted.
            6) pwd -> Displays the current folder.
            7) tree -> Displays the structure of folders and subfolders on the disk from the time folder.
            8) exit -> Exit from the shell.
            9) rmdir <folder_name> -> Deletes the folder named 'folder_name'.
            10) mkdir <folder_name> -> Creates a new 'folder named'.
            11) cd <folder_name> -> Gos to the folder named 'folder_name'.
            12) create <file_name> -> Creates a text file.
            13) type *.* > <file_name> -> Merges all text files in the current folder into one file named 'file_name'.
            14) find *.* <alphanumeric> -> Searches all text files in the current folder for the alphanumeric 'alphanumeric'.
            15) del *.* <alphanumeric> -> Searches all text files for the alphanumeric alphanumeric and deletes it.
