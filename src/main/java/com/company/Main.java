package com.company;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Main {

    public static void main(String[] args) throws IOException {
        Commands cmd = new Commands();
        Path dirPath = Path.of(System.getProperty("user.dir"));
        System.out.println(" ");
        System.out.println("''Simple Shell Project''");
        System.out.println("___________________________");
        System.out.println("-- Write 'list' to see the list of commands!!");
        System.out.println(" ");

        while (true) {
            System.out.print(dirPath + ">");
            Scanner readCommand = new Scanner(System.in);
            String input = readCommand.nextLine();
            List<String> tokens = new ArrayList<>();
            StringTokenizer tokenizer = new StringTokenizer(input, " ");

            while (tokenizer.hasMoreElements()) {
                tokens.add(tokenizer.nextToken());
            }
            if (input.equals("dir")) {
                cmd.dir();
            } else if (input.equals("time")) {
                cmd.time();
            } else if (input.startsWith("copy") && tokens.size() == 4) {
                String destinationFile = tokens.get(3);
                String sourceFile = tokens.get(2);
                cmd.copy(sourceFile, destinationFile);
            } else if (input.equals("type *.*")) {
                cmd.type();
            } else if (input.equals("list")) {
                cmd.list();
            } else if (input.startsWith("sort") && tokens.size() == 2) {
                String filename = tokens.get(1);
                cmd.sort(filename);
            } else if (input.equals("pwd")) {
                cmd.pwd();
            } else if (input.startsWith("find") && tokens.contains("*.*") && tokens.size() == 3) {
                String alphanumeric = tokens.get(2);
                cmd.findAlphanumeric(alphanumeric);
            }else if (input.equals("tree")) {
                cmd.tree();
            }else if (input.startsWith("type") && tokens.size() == 4) {
                String filename = tokens.get(3);
                cmd.typeFileName(filename);
            }else if (input.startsWith("cd") && tokens.size() == 2) {
                String folderName = tokens.get(1);
                Object newPath = cmd.cd(folderName);
                dirPath = (Path) newPath;
            }else if (input.startsWith("mkdir") && tokens.size() == 2) {
                String folderName = tokens.get(1);
                cmd.mkdir(folderName);
            }else if (input.startsWith("del") && tokens.contains("*.*") && tokens.size() == 3) {
                String alphanumeric = tokens.get(2);
                cmd.delAlphanumeric(alphanumeric);
            }else if (input.startsWith("rmdir") && tokens.size() == 2) {
                String folderName = tokens.get(1);
                cmd.rmdir(folderName);
            }else if (input.startsWith("create") && tokens.size() == 2) {
                String filename = tokens.get(1);
                cmd.create(filename);
            }else if (input.equals("exit")) {
                cmd.exit();
            }else {
                System.out.println("** Write 'list' to see all the commands!! **");
            }
        }
    }
}
