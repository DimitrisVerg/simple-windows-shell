package com.company;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Commands {
    public void dir() {
        Path dirPath = Path.of(System.getProperty("user.dir"));
        File folder = new File(String.valueOf(dirPath));
        String[] files = folder.list();
        for (String file : files)
        {
            System.out.println(file);
        }
    }
    public void time(){
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date));
    }
    public void copy(String sourceFile, String destinationFile){
        FileInputStream instream = null;
        FileOutputStream outstream = null;
        Scanner readCommand = new Scanner(System.in);
        Path dirPath = Path.of(System.getProperty("user.dir"));
        Path  checkPath = Path.of(dirPath + "\\" + destinationFile + ".txt");

        try{
            File infile =new File(dirPath + "\\" + sourceFile + ".txt");
            File outfile =new File(dirPath + "\\" + destinationFile + ".txt");

            boolean check = false;
            byte[] buffer = new byte[1024];
            int length;
            if(Files.exists(Paths.get(String.valueOf(checkPath)))) {
                check=true;
            }
            instream = new FileInputStream(infile);
            outstream = new FileOutputStream(outfile);

            if(check==false) {
                while ((length = instream.read(buffer)) > 0) {
                    outstream.write(buffer, 0, length);
                }
                System.out.println("File copied successfully!!");
            }else{
                while (true) {
                    System.out.print("The file exists!!. Do you want to re-copied? (Y/N) : ");
                    String input = readCommand.nextLine();
                    if (input.equals("Y") || input.equals("y")) {
                        while ((length = instream.read(buffer)) > 0) {
                            outstream.write(buffer, 0, length);
                        }
                        System.out.println("File copied successfully!!");
                        break;
                    } else if (input.equals("N") ||  input.equals("n")){
                        System.out.println("File copied cancelled!!");
                    } else {
                        System.out.println("Wrong command. Please write (Y) or (N)!! ");
                    }
                }
            }
            instream.close();
            outstream.close();
        }catch(IOException ioe){
            ioe.printStackTrace();
        }
    }
    public void type(){
        Path dirPath = Path.of(System.getProperty("user.dir"));
        File folder = new File(String.valueOf(dirPath));
        FilenameFilter txtFileFilter = (dir, name) -> {
            if(name.endsWith(".txt"))
            {
                return true;
            }
            else
            {
                return false;
            }
        };
        File[] files = folder.listFiles(txtFileFilter);
        if (files.length>0) {
            for (File file : files) {
                System.out.println(file.getName());
            }
        }else{
            System.out.println("The are no text files!!");
        }
    }
    public void sort(String filename){
        Path dirPath = Path.of(System.getProperty("user.dir"));
        FileInputStream instream = null;
        List<String> numbers = new ArrayList<>();

        try{
            File infile =new File(dirPath + "\\" + filename + ".txt");
            String data = "";
            instream = new FileInputStream(infile);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(instream));
            if(instream!=null){
                try{
                    while((data = bufferedReader.readLine()) != null){
                        numbers.add(data);
                    }
                    instream.close();
                }catch (IOException ieo){
                    ieo.printStackTrace();
                }
            }
            System.out.print("Before Sorting: ");
            for (int i=0; i<numbers.size(); i++){
                System.out.print(numbers.get(i) + " ");
            }
            System.out.println(" ");
            Collections.sort(numbers);
            System.out.print("After Sorting: ");
            for (int i=0; i<numbers.size(); i++){
                System.out.print(numbers.get(i) + " ");
            }
            System.out.println(" ");
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }
    }
    public void pwd(){
        String pwd = System.getProperty("user.dir");
        System.out.println("Current Folder : " + pwd);
    }
    public void findAlphanumeric(String alphanumeric){
        Path dirPath = Path.of(System.getProperty("user.dir"));
        File folder = new File(String.valueOf(dirPath));
        List<File> textFilesList = new ArrayList<>();

        for (File file : folder.listFiles()) {
            if (file.getName().endsWith((".txt"))) {
                textFilesList.add(file);
            }
        }
        for(File textFiles : textFilesList) {
            try {
                Scanner scanner = new Scanner(textFiles);
                int lineNum = 0;
                while (scanner.hasNextLine()) {
                    String line = scanner.nextLine();
                    lineNum++;
                    if(line.contains(alphanumeric)) {
                        System.out.println("The file is : " +textFiles.getName());
                        System.out.println("I found alphanumeric<"+alphanumeric+"> on line : " +lineNum);
                    }
                }
            } catch(FileNotFoundException e) {
            }
        }
    }
    public void tree(){
        Path dirPath = Path.of(System.getProperty("user.dir"));
        File folder = new File(String.valueOf(dirPath));
        File arr[] = folder.listFiles();

        if(folder.exists() && folder.isDirectory())
        {
            RecursivePrintForTreeCommand(arr, 0);
        }
    }
    private void RecursivePrintForTreeCommand(File[] arr, int level) {
        for (File f : arr)
        {
            for (int i = 0; i < level; i++)
                System.out.print("\t");
            if(f.isFile())
                System.out.println(" - " + f.getName());
            else if(f.isDirectory())
            {
                System.out.println("[" + f.getName() + "]");
                RecursivePrintForTreeCommand(f.listFiles(), level + 1);
            }
        }
    }
    public void typeFileName(String fileName) throws IOException {
        String charset = "UTF-8";
        Path dirPath = Path.of(System.getProperty("user.dir"));
        File temp = File.createTempFile(fileName, ".txt");
        PrintWriter writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream(temp), charset));
        File file = new File(String.valueOf(dirPath));

        for (File data : file.listFiles()) {
            if (data.getName().endsWith((".txt"))) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(data), charset));
                for (String line; (line = reader.readLine()) != null;) {
                    writer.println(line);
                }
                reader.close();
                temp.renameTo(new File(fileName));
            }
        }
        writer.close();
    }
    public Object cd(String folderName){
        Path dirPath = Path.of(System.getProperty("user.dir"));
        File directory = new File(folderName).getAbsoluteFile();
        if (directory.exists())
        {
            System.setProperty("user.dir", directory.getAbsolutePath());
            Path newPath = Path.of(System.getProperty("user.dir"));
            return(newPath);
        }else{
            System.out.println("File is not exist!!");
        }
        return dirPath;
    }
    public void mkdir(String folderName){
        Path dirPath = Path.of(System.getProperty("user.dir"));
            new File(String.valueOf(dirPath + "/" + folderName)).mkdir();
    }
    public void delAlphanumeric(String alphanumeric) throws IOException {
        Path dirPath = Path.of(System.getProperty("user.dir"));
        File file = new File(String.valueOf(dirPath));
        String charset = "UTF-8";

        for (File data : file.listFiles()) {
            if (data.getName().endsWith((".txt"))) {
                File temp = File.createTempFile("file", ".txt", data.getParentFile());
                BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(data), charset));
                PrintWriter writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream(temp), charset));

                for (String line; (line = reader.readLine()) != null;) {
                    line = line.replace(alphanumeric, " ");
                    writer.println(line);
                }
                reader.close();
                writer.close();
                data.delete();
                temp.renameTo(new File(data.getName()));
            }
        }
    }
    public void rmdir(String folderName){
        Path dirPath = Path.of(System.getProperty("user.dir"));
        new File(String.valueOf(dirPath + "/" + folderName)).delete();
    }
    public void create(String filename) {
        Path dirPath = Path.of(System.getProperty("user.dir"));
        File file = new File(dirPath + "\\" + filename + ".txt");

        try {
            File myObj = new File(String.valueOf(file));
            if (myObj.createNewFile()) {
                System.out.println("File created: " + myObj.getName());
            } else {
                System.out.println("File already exists.");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void exit(){
        Scanner readCommand = new Scanner(System.in);
        System.out.print("Do you want to exit? (Y/N) : ");
        String input = readCommand.nextLine();
        if (input.equals("Y") || input.equals("y")) {
            System.exit(0);
        }else if (input.equals("N") ||  input.equals("n")){
        System.out.println("Exit Cancelled!!");
        }
    }
    public void list(){
        System.out.println("All the Commands is :");
        System.out.println("  -  dir  ------->  Displays the contents of the current folder.");
        System.out.println("  -  pwd  ------->  Displays the current folder.");
        System.out.println("  -  time ------->  Displays the current date and time on the screen.");
        System.out.println("  -  tree ------->  Displays the structure of folders and subfolders on the disk from the time folder.");
        System.out.println("  -  type *.*  -->  Displays all text files in the current folder.");
        System.out.println("  -  exit  ------>  Exit from the shell.");
        System.out.println("  -  rmdir 'folder_name'  -->  Deletes the folder named 'folder_name'.");
        System.out.println("  -  mkdir 'folder_name'  -->  Creates a new 'folder named'.");
        System.out.println("  -  cd 'folder_name'  ----->  Gos to the folder named 'folder_name'.");
        System.out.println("  -  sort 'file_name'  ----->  Reads 32 binary numbers from the file 'file_name' and displays them sorted.");
        System.out.println("  -  create 'file_name'  -----> Creates a text file.");
        System.out.println("  -  type *.* > 'file_name'  ------------>  Merges all text files in the current folder into one file named\n" +
                "'file_name'.");
        System.out.println("  -  find *.* 'alphanumeric' ------------>  Searches all text files in the current folder for the alphanumeric\n" +
                "'alphanumeric'");
        System.out.println("  -  del *.* 'alphanumeric'  ------------>  Searches all text files for the alphanumeric alphanumeric and deletes it.");
        System.out.println("  -  copy /+Y 'source_file' 'destination_file'  ---->  Copies the source file to the destination file.");
    }
}
